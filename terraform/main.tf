terraform {
  backend "s3" {
    bucket = "siu-terraform-state"
    key    = "terraform/state"
    region = "us-east-1"
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.22.0"
    }
  }
}

provider "aws" {
  region = var.region
}

//
// Variables
//

variable "ami" {}
variable "region" {}
variable "vpc_id" {}
variable "env_name" {}
variable "ssh_admin_ip" {}
variable "ssh_key_name" {}
variable "instance_type" {}

locals {
  deploy_script = [
    "#!/bin/bash",
    replace("scp ./build/* ec2-user@xxx:~", "xxx", aws_eip.express-web-api-eip.public_dns),
    replace("ssh ec2-user@xxx 'sudo forever stopall'", "xxx", aws_eip.express-web-api-eip.public_dns),
    replace("ssh ec2-user@xxx 'sudo npm install'", "xxx", aws_eip.express-web-api-eip.public_dns),
    replace("ssh ec2-user@xxx 'sudo forever start api.js'", "xxx", aws_eip.express-web-api-eip.public_dns),
    replace("ssh ec2-user@xxx 'sudo forever list'", "xxx", aws_eip.express-web-api-eip.public_dns)
  ]
  depends_on = [
    aws_eip.express-web-api-eip
  ]
}

//
// Resources
//

resource "aws_eip" "express-web-api-eip" {
  instance = aws_instance.express-web-api.id
  vpc      = true
  tags = {
    Name = "siu-dev-eip-express-web-api"
  }
  depends_on = [
    aws_instance.express-web-api
  ]
}

resource "aws_instance" "express-web-api" {
  ami           = var.ami
  key_name      = var.ssh_key_name
  instance_type = var.instance_type
  user_data     = file("user-data.sh")

  vpc_security_group_ids = [aws_security_group.express-web-api-security-group.id]

  tags = {
    Name = "siu-dev-express-web-api"
  }

  depends_on = [
    aws_security_group.express-web-api-security-group
  ]
}

resource "aws_security_group" "express-web-api-security-group" {
  name        = "siu-dev-sg-express-web-api"
  description = "Allow inbound web traffic"
  vpc_id      = var.vpc_id

  ingress {
    description = "Inbound web traffic"
    from_port   = 5150
    to_port     = 5150
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Inbound admin traffic"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.ssh_admin_ip]
  }

  egress {
    description = "Outbound secure web traffic"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "siu-dev-sg-express-web-api"
  }
}

resource "local_file" "deploy_script" {
  filename = "deploy.sh"
  content  = join("\n", local.deploy_script)
  depends_on = [
    aws_instance.express-web-api,
    aws_eip.express-web-api-eip
  ]
}

# Dirty timer: Wait for the user-data.sh script to complete (first spin-up only).
resource "time_sleep" "wait_3_minutes" {
  depends_on = [
    aws_instance.express-web-api
  ]
  create_duration = "3m"
}

//
// Output
//

output "ec2_info" {
  value = aws_instance.express-web-api
  depends_on = [
    aws_instance.express-web-api
  ]
}

output "eip_info" {
  value = aws_eip.express-web-api-eip.public_dns
  depends_on = [
    aws_eip.express-web-api-eip
  ]
}
