const { v4: uuidv4 } = require('uuid');

module.exports.getId = () => {
    return uuidv4();
}
