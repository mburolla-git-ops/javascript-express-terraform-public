const cors = require('cors');
const express = require('express');
const identification = require ('./identification');

const PORT = 5150;
const app = express();

const corsOptions = {
    origin: 'http://localhost:3000',
    optionsSuccessStatus: 200
}

// Middleware...
app.use(cors());
app.use(express.json());
app.use(express.urlencoded());

// Endpoints...
app.get('/health', cors(corsOptions), (req, res) => {
    res.send("OK");
});

app.get('/id', cors(corsOptions), (req, res) => {
    res.send(`Here is your ID: ${ identification.getId() }`);
});

app.listen(PORT, () => {
    console.log(`Express Server is running on port: ${PORT}`);
});
