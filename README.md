# JavaScript Express Web API
Builds and deploys an Express web API to an AWS S3 bucket using a GitLab pipeline with Terraform.

# Getting Started
- Clone this repo
- Install and run [Docker Desktop](https://www.docker.com/products/docker-desktop/)
- Add these variables and their associated values to GitLab (Settings >> CI/CD):
  - AWS_ACCESS_KEY_ID
  - AWS_SECRET_ACCESS_KEY
  - AWS_DEFAULT_REGION
  - SSH_PRIVATE_KEY
- Pipeline runs when changes are detected on the `dev` branch

# Overview
![](./docs/cicd_overview.png)

# API Endpoints
- [ThunderClient Collection](./docs/thunder-collection_API.json)
- GET http://<<YOUR EIP>>:5150/id
- GET http://<<YOUR EIP>>:5150/health

# GitLab Pipeline Links
- [.gitlab-ci.yml Full Reference](https://docs.gitlab.com/ee/ci/yaml/index.html)
- [Predefined Variables Reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)
- [.gitlab-ci](https://docs.gitlab.com/ee/ci/yaml/gitlab_ci_yaml.html)
- [CI/CD Variables](https://about.gitlab.com/blog/2021/04/09/demystifying-ci-cd-variables/)
- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [Tutorials](https://docs.gitlab.com/ee/tutorials/)
- [Example Templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates)

# GitLab Runner

#### Download GitLab Runner Binary
- Open Powershell as admin
- Download and save runner binary in your Windows *user* directory
- `Invoke-WebRequest -Uri "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-windows-amd64.exe" -OutFile "gitlab-runner.exe"`

#### Register the runner:
- `gitlab-runner.exe register`
  - Copy token from GitLab web console
  - Use the `docker` executor option
- `gitlab-runner.exe install`
- `gitlab-runner.exe start`
- `gitlab-runner.exe verify`
- GitLab Website
  - Make sure the dot is green
  - Turn off shared runners slider to use your runner

# Terraform
- Create Powershell Alias for Terraform: `New-Alias -Name "tf" -Value "terraform"`
- [https://registry.terraform.io/](https://registry.terraform.io/)
- `tf destroy -var-file ./env-vars/dev.tfvars -auto-approve`
