const identification = require('../src/identification');

test('Identification UUID', () => {
    const uuid = identification.getId();
    expect(uuid.length).toBe(36);
});
